package com.example.bobsburgersapp.view

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.bobsburgersapp.R
import com.example.bobsburgersapp.databinding.CharacterItemBinding
import com.example.bobsburgersapp.model.local.CharacterEntity

class CharacterAdapter(
    val onSetFavorite:(CharacterEntity)-> Unit,
    private val context: Context
        ) : RecyclerView.Adapter<CharacterViewHolder>() {

    private var list: MutableList<CharacterEntity> = mutableListOf()

    fun updateList(newList: List<CharacterEntity>) {
        //removing whole
       val oldSize = list.size
       list.clear()
       notifyItemRangeRemoved(0, oldSize)
       list.addAll(newList)
       notifyItemRangeInserted(0, newList.size)

    }


    private fun setFavoriteButton(button: ImageView, position: Int){
        with(list[position]){
            button.setImageResource(if (liked) R.drawable.ic_favorite_filled else R.drawable.ic_favorite_border)
            button.setOnClickListener{
                with(list[position]) {
                    button.setImageResource(if (liked) R.drawable.ic_favorite_border else R.drawable.ic_favorite_filled)
                    liked = !liked
                    // Run block which Update the favorite character in character_table
                    onSetFavorite(this)
                }

            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        return CharacterViewHolder(
            CharacterItemBinding.inflate(LayoutInflater.from(parent.context))
        )
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        holder.displayCharacter(list[position])
        with(holder){
            setFavoriteButton(favoriteBtn,position)
        }
    }

    override fun getItemCount(): Int = list.size
}

class CharacterViewHolder(val binding: CharacterItemBinding) :
    RecyclerView.ViewHolder(binding.root) {
    var favoriteBtn = binding.icFavorite
    fun displayCharacter(character: CharacterEntity) {
        // TODO: display info about character
        binding.charImg.load(character.image)
        binding.name.text = character.name
        binding.age.text = character.age
        binding.gender.text = character.gender

    }
}

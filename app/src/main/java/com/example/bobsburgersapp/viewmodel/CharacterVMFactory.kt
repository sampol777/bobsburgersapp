package com.example.bobsburgersapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.bobsburgersapp.model.BurgersRepo

class CharacterVMFactory(private val repo: BurgersRepo)
    :ViewModelProvider.NewInstanceFactory(){
        override fun <T : ViewModel> create (ModelClass:Class<T>): T {
            return BurgersViewModel(repo) as T
        }
}
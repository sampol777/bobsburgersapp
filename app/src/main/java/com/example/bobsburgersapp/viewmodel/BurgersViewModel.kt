package com.example.bobsburgersapp.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bobsburgersapp.model.BurgersRepo
import com.example.bobsburgersapp.model.local.CharacterEntity
import kotlinx.coroutines.launch

class BurgersViewModel(val repo: BurgersRepo) : ViewModel() {
    private val _state: MutableLiveData<BurgerState> =  MutableLiveData(BurgerState())
            val state:LiveData<BurgerState> get() = _state

    fun getChars(count : Int) {
        viewModelScope.launch {
            _state.value = BurgerState(isLoading = true)
            val result = repo.getCharacters(count)
            Log.e("TAG", "getChars: length: ${result.size} $result !!!!!!!!!!!!!", )
            _state.value = BurgerState(items = result, isLoading = false)
        }
    }

    fun setFavorite(character:CharacterEntity){
        viewModelScope.launch {
            val updatedList = _state.value?.items
            Log.e("TAG", "setFavorite: CLIICKEE", )
            repo.updateCharacter(character)
        }
    }



    data class BurgerState(
        val isLoading: Boolean = false,
        val items: List<CharacterEntity> = emptyList()
    )
}
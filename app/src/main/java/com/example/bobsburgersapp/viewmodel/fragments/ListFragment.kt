package com.example.bobsburgersapp.viewmodel.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.example.bobsburgersapp.databinding.CharacterItemBinding
import com.example.bobsburgersapp.databinding.FragmentListBinding
import com.example.bobsburgersapp.model.BurgersRepo
import com.example.bobsburgersapp.view.CharacterAdapter
import com.example.bobsburgersapp.viewmodel.BurgersViewModel
import com.example.bobsburgersapp.viewmodel.CharacterVMFactory

class ListFragment : Fragment() {
    private lateinit var binding: FragmentListBinding
    private lateinit var vmFactory: CharacterVMFactory
    private val viewModel by viewModels<BurgersViewModel>() { vmFactory }

    private val theAdapter: CharacterAdapter by lazy {
        CharacterAdapter(viewModel::setFavorite,requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentListBinding.inflate(inflater, container, false)
        vmFactory = CharacterVMFactory(BurgersRepo(requireContext()))
        initViews()
        initObservers()
        return binding.root
    }

    fun initViews() {
        //<editor-fold desc="Init RV">
        with(binding.charachter) {
            layoutManager = GridLayoutManager(context, 1)
            adapter = theAdapter
        }
        //</editor-fold>
        //<editor-fold desc="Listen for clicks">
        binding.getMore.setOnClickListener {
            viewModel.getChars(10)
        }
        //</editor-fold>
        //<editor-fold desc="Init Observer and get chard to trigger state update">

        viewModel.getChars(20)
        //</editor-fold>
    }

    fun initObservers(){
        viewModel.state.observe(viewLifecycleOwner){ state ->
            Log.e("tag", "initObservers: SOMETHING CHANGED ${state.items}", )
            binding.foreverSpinner.isVisible = state.isLoading
            theAdapter.updateList(state.items)
        }
    }
}
package com.example.bobsburgersapp.model.local

import androidx.room.*

@Dao
interface CharacterDao {
    @Query("SELECT * FROM CharacterEntity")
    suspend fun getAll(): List<CharacterEntity>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertCharacter(vararg character: CharacterEntity)

    @Update
    suspend fun updateCharacter(character:CharacterEntity)



}
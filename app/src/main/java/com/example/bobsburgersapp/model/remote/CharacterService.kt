package com.example.bobsburgersapp.model.remote

import com.example.bobsburgersapp.model.local.CharacterEntity
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Path

interface CharacterService {
    companion object {
        private const val BASE_URL: String = "https://bobsburgers-api.herokuapp.com"
        private const val ENDPOINT = "/characters/"
        private const val CHARACTERS_ENDPOINT = "$ENDPOINT{ids}"
        fun getInstance(): CharacterService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create()

    }

    @GET(CHARACTERS_ENDPOINT)
    suspend fun getCharacters(@Path("ids") ids: List<Int>): List<CharacterEntity>


    fun getListOf20(): MutableList<Int> {
        val s: MutableSet<Int> = mutableSetOf()
        while (s.size < 20) {
            s.add((0..506).random())
        }
        var randomListt = s.toMutableList()
        return randomListt
    }

//    fun getRandomInts(count: Int): Set<Int> {
//        val idRange = 0..506
//        val result = mutableSetOf<Int>()
//        while (result.size < count) {
//            result.add(idRange.random())
//        }
//        return result
//    }


}

fun getRandInts(count: Int): List<Int> = mutableSetOf<Int>().run {
    while (size < count) {
        add((0..506).random())
    }
    toList()
}
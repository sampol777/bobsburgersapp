package com.example.bobsburgersapp.model

import android.content.Context
import android.util.Log
import com.example.bobsburgersapp.model.local.CharacterDB
import com.example.bobsburgersapp.model.local.CharacterEntity
import com.example.bobsburgersapp.model.remote.CharacterService
import com.example.bobsburgersapp.model.remote.getRandInts
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class BurgersRepo(context: Context) {
    private val characterService = CharacterService.getInstance()
    private val characterDao = CharacterDB.getInstance(context).characterDao()

    suspend fun getCharacters(count:Int): List<CharacterEntity> =
        withContext(Dispatchers.IO) {
            val cachedCharacters: List<CharacterEntity> = characterDao.getAll()
            if(count == 10){
                val randIds = getRandInts(count)
                val remoteChar: List<CharacterEntity> = characterService.getCharacters(randIds)
                val newList:List<CharacterEntity> = cachedCharacters + remoteChar
                Log.e("TAG", "getCharacters: ${newList.size}", )
                characterDao.insertCharacter(*remoteChar.toTypedArray())
                return@withContext cachedCharacters + remoteChar
            }
            return@withContext cachedCharacters.ifEmpty {
                val randIds = getRandInts(count)
                val remoteChar: List<CharacterEntity> = characterService.getCharacters(randIds)
                characterDao.insertCharacter(*remoteChar.toTypedArray())
                return@ifEmpty remoteChar
            }
        }

    suspend fun updateCharacter(character:CharacterEntity) = withContext(Dispatchers.IO){
        characterDao.updateCharacter(character)
    }
}
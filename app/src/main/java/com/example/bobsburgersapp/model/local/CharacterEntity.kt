package com.example.bobsburgersapp.model.local

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import com.google.gson.Gson

@Entity
data class CharacterEntity(
    @PrimaryKey val id: Int = 0,
    val age: String = "",
    val firstEpisode: String = "",
    val gender: String = "",
    val image: String = "",
    val name: String = "",
    val occupation: String = "",
    val relatives: List<CharacterEntity> = emptyList(),
    val url: String = "",
    val voicedBy: String = "",
    val wikiUrl: String = "",
    val relationship: String = "",
    var liked:Boolean = false
)

class Converters {

    @TypeConverter
    fun listToJson(list: List<CharacterEntity>?): String = Gson().toJson(list)

    @TypeConverter
    fun jsonToList(value: String): List<CharacterEntity> =
        Gson().fromJson(value, Array<CharacterEntity>::class.java).toList()
}